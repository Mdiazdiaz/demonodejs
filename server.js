var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var path = require('path');

  var clientes = [{'nombre':'Miguel'},{'nombre':'Pepe'},{'nombre':'Juan'}];
  app.listen(port);

  console.log('todo list RESTful API server started on: ' + port);
/* Ejemplo anterior.
app.get('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));

});
*/
// SI EJECUTAS LA SIGUIENTE URL, TE MUESTRA EN EL GET EL HTML DEL FICHERO NUEVO
app.get('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));

});

// TE FACILITA EL NÚMERO QUE LE INTRODUZCAS POR URL. POR EJEMPLO: http://localhost:3000/clientes/14
app.get('/clientes/:idcliente', function(req,res){
  res.json({ message: 'Hemos recibido la peticion cambiada sobre el json de clientes ' + clientes[req.params.idcliente].nombre});
});


app.get('/clientes/:idcliente', function(req,res){
  res.json({ message: 'Task successfully deleted' });
});

app.get('/clientes/:idcliente', function(req,res){
  res.json({ message: 'Task successfully deleted' });
});

app.post('/clientes/:idcliente',function(req, res){
 res.json({ message: 'El cliente' + req.params.idcliente + ' ha sido creado' });

});

app.put('/clientes/:idcliente',function(req, res){
 res.json({ message: 'El cliente' + req.params.idcliente + ' ha sido actualizado' });

});
