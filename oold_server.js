var express = require('express'),
 app = express(),
 port = process.env.PORT || 3000;

 var path=require ('path');


var clientes = [{'nombre':'carlos'},{'nombre':'antonio'},{'nombre':'luis'}]
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res){
res.sendFile(path.join(__dirname,'index.html'));

});

app.post('/', function(req,res){
  res.send('Hemos recibido tu peticion');

});

app.get('/clientes',function(req, res){

 res.sendFile(path.join(__dirname,'index.html'));

});


app.get('/clientes/:idcliente',function(req, res){
 res.json({ message: 'Hemos recibido la peticion sobre el cliente ' + req.params.idcliente + ' ' + clientes[req.params.idcliente].nombre});

});


app.post('/clientes/:idcliente',function(req, res){
 res.json({ message: 'El cliente' + req.params.idcliente + ' ha sido creado' });

});

app.put('/clientes/:idcliente',function(req, res){
   res.json({ message: 'El cliente' + req.params.idcliente + ' ha sido actualizado' });
});

app.delete('/clientes/:idcliente',function(req, res){
 res.json({ message: 'El cliente' + req.params.idcliente + ' ha sido borrado' });
});
